node ('epics-deployment') {

    docker.image('europeanspallationsource/centos71-epics:2.1.1').inside(
        "-v /opt/epics:/opt/epics:rw -v /opt/eldk-5.6:/opt/eldk-5.6:ro -v ${env.HOME}/.bitbucket.cfg:${env.HOME}/.bitbucket.cfg:ro") {

        repo_name = repo_url.split('/').last()

        try {
            stage('Download git') { // for display purposes
                bbStatus('INPROGRESS')
                // Get some code from a GitHub repository
                print "REPO: ${repo_url} , TAG: ${git_tag}, REPO_NAME: ${repo_name}, USERNAME: ${username}"
                sh "env | grep EPICS"
                dir ("${repo_name}") {
                    git url: "${repo_url}", credentialsId: "${git_credentials_id}"
                }
                shdir "git remote -v"
                shdir "git branch -a"
                shdir "git log -1 --decorate ${git_tag}"
                shdir "git checkout ${git_tag}"
            }

            stage('Build') {
                shdir "make help"
                shdir "time make"
                version = shdir("make version", true).trim()
                // make version returns the username when the git repository is dirty
                // we can't check "v${version}" != git_tag because make version automatically
                // adds 0 to the tag if needed (tag v2.0 is transformed to version 2.0.0)
                if("${version}" == env.USER) {
                    error("Invalid version: make version returned ${version} but tag is ${git_tag}")
                }
            }

            stage('Installation to EEE') {
                shdir "make install"
                bbStatus('SUCCESSFUL')
            }

        } catch (e) {
            bbStatus('FAILED')
            deleteRepoDir()
            //hipchatSend (color: 'RED', notify: true, message: "FAILED: Job <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a> repo: ${repo_name} / tag: ${git_tag} / username: ${username}")
            slackSend (color: 'danger', message: "FAILED: Job <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]> repo: ${repo_name} / tag: ${git_tag} / username: ${username}")
            throw e
        }

        deleteRepoDir()
        //hipchatSend (color: 'GREEN', message: "SUCCESSFUL: Job <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a> repo: ${repo_name} / tag: ${git_tag} / username: ${username}")
        slackSend (color: 'good', message: "SUCCESSFUL: Job <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]> repo: ${repo_name} / tag: ${git_tag} / username: ${username}")
    }
}


// dir('foo') inside "docker.image().inside{}" does not affect CWD of launched processes
// https://issues.jenkins-ci.org/browse/JENKINS-33510
// shdir is a workaround to run a command inside repo_name directory
def shdir(command, returnStdout=false) {
    // Use --login to source /etc/profile.d/ess_epics_env.sh
    sh script: "cd ${repo_name} && bash --login -c \"${command}\"", returnStdout: returnStdout
}

def bbStatus(set_build_status) {
    // def commitId = sh(returnStdout: true, script: "git rev-list -1 ${git_tag}").trim()
    sh "/usr/local/bin/bitbucket.pex build_status --revision ${git_hash} --state ${set_build_status} --key JENKINS-BUILD-${BUILD_NUMBER} --url ${JOB_URL}${BUILD_NUMBER} ${repo_name}"
}

def deleteRepoDir() {
    dir ("${repo_name}") {
        deleteDir()
    }
}
