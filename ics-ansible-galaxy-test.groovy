library 'ics'

node ('molecule') {

    repo_name = repo_url.split('/').last().replace('.git', '')
    short_commit_id = git_hash.take(8)

    currentBuild.displayName = repo_name

    try {
        stage('Clone') {
            updateCommitStatus('INPROGRESS', repo_url, git_hash)
            print "REPO: ${repo_url} , COMMIT: ${git_hash}, REPO_NAME: ${repo_name}, USERNAME: ${username}, SLACK_ENABLED: ${slack_enabled}"
            dir ("${repo_name}") {
                git url: "${repo_url}", credentialsId: "${git_credentials_id}"
                sh "git log -1 --decorate ${git_hash}"
                sh "git checkout ${git_hash}"
            }
        }

        molecule_path = moleculePath(repo_name)
        if (molecule_path == '') {
            currentBuild.result = 'UNSTABLE'
            updateCommitStatus('CANCELED', repo_url, git_hash)
            return
        }

        withEnv(['PATH+MOLECULE_PATH=' + molecule_path]) {
            stage('Create') {
                dir ("${repo_name}") {
                    if (env.PATH.startsWith('/opt/conda/envs/molecule2/bin')) {
                        sh 'molecule lint'
                    }
                    sh 'molecule destroy'
                    sh 'molecule dependency'
                    sh 'molecule syntax'
                    sh 'molecule create'
                }
            }

            stage('Converge') {
                dir ("${repo_name}") {
                    sh 'molecule converge'
                }
            }

            stage('Test') {
                dir ("${repo_name}") {
                    sh 'molecule idempotence'
                    sh 'molecule verify'
                    updateCommitStatus('SUCCESSFUL', repo_url, git_hash)
                }
            }
        }

        stage('Clean') {
            clean()
        }

    } catch (e) {
        updateCommitStatus('FAILED', repo_url, git_hash)
        slackSend (color: 'danger', message: "FAILED: Job <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]> repo: ${repo_name} / commit: ${short_commit_id} / username: ${username}")
        clean()
        throw e
    }

    if (slack_enabled == "true") {
        slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]> repo: ${repo_name} / commit: ${short_commit_id} / username: ${username}")
    }

}


def clean() {
    withEnv(['PATH+MOLECULE_PATH=' + molecule_path]) {
        dir ("${repo_name}") {
            sh 'molecule destroy'
            deleteDir()
        }
    }
}
