node ('bitbucket-backup') {

    slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")

    try{
        stage('Backup bitbucket') {
            withCredentials([usernamePassword(credentialsId: 'bitbucket-read-only-key-secret',
                                              usernameVariable: 'OAUTH_KEY', passwordVariable: 'OAUTH_SECRET')]) {
                sh '/opt/conda/envs/bitbucket-backup/bin/bitbucket-backup -k $OAUTH_KEY -s $OAUTH_SECRET --team europeanspallationsource --verbose --attempts 5 --with-wiki --mirror --location /opt/backup/bitbucket'
            }
        }
    } catch (e) {
        slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        throw e
    }

    slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")

}
